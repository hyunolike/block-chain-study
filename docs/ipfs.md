## 블록체인 기술 + 분산형 웹 `IPFS (InterPlanetary File System, 분산형 파일 시스템)`
> [참고자료](https://www.lgcns.com/blog/it-trend/31193/)
- NFT(대체불가능토큰) >> 블록체인상 저장
    - 실제 영상, 이미지와 같은 데이터 존재 x
    - 실제 원본 데이터 >> 특정 서버 저장!! ✅

### 1. HTTP - `찾을 위치` 시스템 전달
- 인터넷의 중요한 특징 >> `중앙화 구조`
    - 가장 많이 사용! `HTTP`: URL 이란 특정 주소 입력 >> **위치 기반** 으로 데이터 접근
    - 따라서, 데이터 관리 및 용량 늘리기 등 작업 수월
- `HTTP` 보안 및 효율성 측면의 단점
    - 데이터에 대한 검열! >> 데이터를 수집, 저장한 주체가 임의로 하게됨!
    - 백본 네트워크의 의존도 높음! 비효율적이며 공격의 대상 쉬움 ㅠ,ㅠ 
    - 백본 네트워크란 연결돼있는 소형 회선들로부터 데이터 수집해 빠르게 전송할 수 있는 대규모 전송회선

### 2. 비교!
![](https://www.lgcns.com/wp-content/uploads/2022/05/image-2.png)

### 😛 3. IPFS - `찾고 있는 대상` 시스템 전달
- P2P 네트워크 접속하는 모든 이용자 >> 완전히 분산된 방식! 데이터 저장, 배포하는 시스템
- `해시`: 사용자가 데이터 찾을 수 있도록 할당!
    - 해시 >> 다양한 길이를 가진 데이터를 고정된 길이를 가진 데이터로 매핑하는 것
