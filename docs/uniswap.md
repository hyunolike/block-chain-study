## 유니스왑
> [참고자료](https://medium.com/@aiden.p/uniswap-series-1-%EC%9C%A0%EB%8B%88%EC%8A%A4%EC%99%91-%EC%9D%B4%ED%95%B4%ED%95%98%EA%B8%B0-e321446623c7)

### 1. DEX (Decentralized Exchange)
- 암호화폐를 거래할 수 있는 __탈중앙 거래소__
- (기존) 오더북 기반 DEX >> (개선) 유니스왑 기반 DEX

### 2. AMM 알고리즘
> Automated Market Maker
- 사용자들의 거래를 AMM 알고리즘 기반으로 자동으로 체결하도록 만듬
    - 누구나 자유롭게 유동성 공급
    - 유동성 바탕, 알고리즘으로 결정된 가격으로 언제든지 사용자들 거래 가능해짐

