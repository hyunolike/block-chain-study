```solidity
pragma solidity >=0.4.24 <0.5.6;

contract Pratice { // smart contract name
    string public name = "KlayLion";
    string public symbol = "KL";

    mapping (uint256 => address) public tokenOwner;
    mapping (uint256 => string) public tokenURIs;

    // 소유한 토큰 리스트
    mapping(address => uint256[]) private _ownedTokens;

    // mint(tokenId, uri, owner) 발행
    // transferFrom(from, to, tokenId) 전송 -> owner가 바뀌는 것(from -> to)

    function mintWithTokenURI(address to, uint256 tokenId, string memory tokenURI) public returns (bool) {
        // to에게 tokenI(일련번호)를 발행하겠다아아아
        // 적힐 글자는 tokenURI ~~
        tokenOwner[tokenId] = to;
        tokenURIs[tokenId] = tokenURI;

        // add token to the list
        _ownedTokens[to].push(tokenId);

        return true;
    }

    function safeTransferForm(address from, address to, uint256 tokenId) public {
        require(from == msg.sender, "from != msg.sender"); // 보낸 사람이 같을 때 
        require(from == tokenOwner[tokenId], "you are not the owner of the token");

        _removeTokenFromList(from, tokenId);
        _ownedTokens[to].push(tokenId);
        tokenOwner[tokenId] = to;
    }

    function _removeTokenFromList(address from, uint256 tokenId) private {
        // [10, 15, 19, 20] -> 19번을 삭제할래
        // [10, 15, 20, 19] -> 자리 바꿔
        // [10, 15, 20] -> 길이 줄이기 >> 삭제 된거야 ㅋㅋㅋㅋ
        uint256 lastTokenIndex = _ownedTokens[from].length - 1;
        for(uint256 i=0; i<_ownedTokens[from].length;i++){
            if(tokenId == _ownedTokens[from][i]) {
                // swap last token with deleting token;
                _ownedTokens[from][i] = _ownedTokens[from][lastTokenIndex];
                _ownedTokens[from][lastTokenIndex] = tokenId;
                break;
            }
        }
        _ownedTokens[from].length--;
    }

    function ownedTokens(address owner) public view returns (uint256[] memory) {
        return _ownedTokens[owner];
    }

    function setTokenUri(uint256 id, string memory uri) public {
        tokenURIs[id] = uri;
    }
}
```